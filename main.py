import json
import sys
import argparse
import os
import shutil


class ChordProcessor:
    def process_chord(self, chord, ch_out):
        pass


def check_all_characters_present(string, string_list):
    for s in string_list:
        if set(s).issuperset(set(string)):
            return s
    return None

class DuplicateChordFinder(ChordProcessor):
    def __init__(self):
        super().__init__()
        self.chords = []

    def process_chord(self, chord, ch_out):
        collision = check_all_characters_present(chord, self.chords)
        if collision:
            raise ValueError('Duplicate chord: ' + chord + ' collides with ' + collision)
        self.chords.append(chord)

    def get_chord_count(self):
        return len(self.chords)
        

class LayoutTranslator(ChordProcessor):
    def __init__(self, out, target_chars):
        self.out = out
        self.target_chars = target_chars

    def process_chord(self, chord, ch_out):
        chord_chars = []
        for c in chord:
            chord_chars.append(self.target_chars[c].replace(' ', '+'))
        trans_target = []
        for c in ch_out:
            trans_target.append(self.target_chars[c])
        # print("Translating: " + chord + " ⇒ " + ch_out)
        self.out.write("# " + chord + " => " + ch_out + "\n")
        self.out.write('+'.join(chord_chars) + " = macro(" + ' '.join(trans_target) + ")\n")


class ChordReader:
    def read_chords(self):
        pass


class ChordReaderMysql(ChordReader):
    def __init__(self, config, chord_processors):
        self.config = config
        self.chord_processors = chord_processors

    def read_chords(self):
        import configparser
        cp = configparser.ConfigParser()
        cp.read(self.config)
        mysql_config = cp['mysql']
        import mysql.connector
        from mysql.connector import errorcode
        try:
            cnx = mysql.connector.connect(**mysql_config)
            cursor = cnx.cursor()
            query = ("select ch, res from chord")
            cursor.execute(query)
            for (chord, expected_result) in cursor:
                for processor in self.chord_processors:
                    processor.process_chord(chord, expected_result)
            cursor.close()
        except mysql.connector.Error as err:
            if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                print("Something is wrong with your user name or password")
            elif err.errno == errorcode.ER_BAD_DB_ERROR:
                print("Database does not exist")
            else:
                print(err)
        else:
            cnx.close()


class ChordReaderJsonFile(ChordReader):
    def __init__(self, filename, chord_processors):
        self.filename = filename
        self.chord_processors = chord_processors

    def read_chords(self):
        f = open(self.filename, "r")
        chords = json.load(f)
        f.close()
        for chord, expected_result in chords.items():
            for processor in self.chord_processors:
                processor.process_chord(chord, expected_result)


def parser():
    # if argv is None, uses the sys.argv[1:]
    parser = argparse.ArgumentParser(
        prog='keyd-generator',
        description='A configuration generator for keyd')
    parser.add_argument('-l', '--layout',
                        help='The target layout on the machine, described in a json file with the same name')
    parser.add_argument('-t', '--template', help='The basic template to which extra chords are added')
    parser.add_argument('-s', '--source', help='Where to get the chord information from')
    return parser.parse_args()


def main(options):
    print(options.layout)
    print(options.template)
    print(options.source)

    base_layout = "qwerty"
    # reading qwerty layout
    bl = open("layouts/" + base_layout + ".json", "r")
    base_json = json.load(bl)
    bl.close()

    # get the qwerty basic keymap in the form: [key name, [characters]] (only the first character is interesting for us)
    tl = open("layouts/" + options.layout + ".json", "r")
    target_json = json.load(tl)
    tl.close()

    # [desired char, raw key presses in the base layout as registered by keyd, including keyd modifiers]
    target_chars = {}
    # [dead key, raw base keys] - ew: "*¨" => "G-'"
    all_dead_keys = {}
    for keycode, chars in target_json["keymap"].items():
        # Guild the string of actual characters that a keycode outputs - alone and along with modifiers
        for i, c in enumerate(chars):
            # the order in which those appear represent: the key itself, then shift, then altgr, and then shift+altgr
            output = "" if i == 0 else "S-" if i == 1 else "G-" if i == 2 else "G-S-"
            actual_target = c
            if c in target_json["deadkeys"]:
                actual_target = target_json["deadkeys"][c][c]
            base_char = base_json["keymap"][keycode][0]
            if base_char == " ":
                base_char = "space"
            if base_char in base_json["deadkeys"]:
                # that "character" is actually a dead key, replace it by the key itself instead of the deadkey
                actual_key = base_json["deadkeys"][base_char][base_char]
                if c in target_json["deadkeys"]:
                    # register it as a deadkey for further processing (all characters obtained through this deadkey)
                    all_dead_keys[c] = output + actual_key
                # also register the corresponding character on its own
                target_chars[actual_target] = output + actual_key
            else:
                # not a deadkey, simply register the character and its equivalent char in the base layout
                #print(c + " ⇏ " + output)
                target_chars[actual_target] = output + base_char
    # add all characters that can be obtained through deadkeys
    for deadkey, kc_out in all_dead_keys.items():
        for second_char, rez in target_json["deadkeys"][deadkey].items():
            if second_char != deadkey and second_char != " " and second_char != " " and second_char != " " and rez not in target_chars:
                # ignore when it's the dead key or some spaces
                # print(rez + " ⇒ " + output + " " + second_char)
                # get the desired output for the second char, which shouldn't be a deadkey
                c = target_chars[second_char]
                target_chars[rez] = kc_out + " " + ("space" if c == " " else c)

    # reading source layout
    outfile = f'output.conf' # "/etc/keyd/default.conf"
    shutil.copy(options.template, outfile)
    of = open(outfile, "a")
    duplicateChordFinder = DuplicateChordFinder()
    chord_processors = [duplicateChordFinder, LayoutTranslator(of, target_chars)]
    if options.source.startswith("mysql:"):
        chord_reader = ChordReaderMysql(options.source[len("mysql:"):], chord_processors)
    else:
        chord_reader = ChordReaderJsonFile(options.source, chord_processors)
    chord_reader.read_chords()
    of.close()
    if duplicateChordFinder.get_chord_count() >= 256:
        print('##### WARNING ###### 256 chords or more, make sure to patch keyd')
    print('%s chords processed.' % duplicateChordFinder.get_chord_count())


if __name__ == "__main__":
    args = parser()
    main(args)
