# keyd-generator

A tool to generate keyd files of chords from a given set of chords, in a specific target keyboard layout.

Layouts can be retrieved at https://github.com/Nuclear-Squid/ergol/tree/master/layouts